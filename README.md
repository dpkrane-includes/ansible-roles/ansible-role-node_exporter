# Ansible Prometheus Node Exporter role.

Роль устанавливает последнюю версию Prometheus Node Exporter с GitHub.

  ## По умолчанию используются следующие переменные:
    node_exporter_user: node_exporter
    node_exporter_group: node_exporter
    node_exporter_tmp_dir: /tmp
    node_exporter_bin: /usr/local/bin/node_exporter
    node_exporter_port: 9100
